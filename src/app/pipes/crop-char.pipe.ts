import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cropChar'
})
export class CropCharPipe implements PipeTransform {

  private readonly STRING_LEN = 130;
  transform(value: string, ...args: unknown[]): unknown {
    return value.length > this.STRING_LEN ? value.substr(0, this.STRING_LEN) + '...' : value;
  }

}
