import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Store, select } from '@ngrx/store';
import { WebStorageService } from 'src/app/services/web-storage.service';
import { DisplayData, ESelectedItems } from 'src/app/model/types-interfaces';
import { AppState } from 'src/app/store/app.state';
import { initialFilterState } from 'src/app/store/filter/filter.state';
import { selectFilter } from 'src/app/store/filter/filter.selector';
import { InitFavorite } from 'src/app/store/favorite/favorite.actions';
import { GetItems, SearchItems } from 'src/app/store/gallery/gallery.actions';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndexComponent implements OnInit, OnDestroy {

  private _filter = '';
  public isMaxPage = false;
  public isFavorite = initialFilterState.currentSelect === ESelectedItems.favorite;
  private _subscriptions: Subscription[] = [];
  private readonly _onStorageChange: (event: StorageEvent) => void = (event) => {
    if (event.key === WebStorageService.FAVORITE) {
      const items: DisplayData[] = JSON.parse(event.newValue) as DisplayData[];
      this._store.dispatch(new InitFavorite(items));
    }
  }


  constructor(private _api: ApiService, private _store: Store<AppState>, private _webStore: WebStorageService) {

  }

  ngOnInit(): void {
    this._subscriptions.push(
      this._store.pipe(select(selectFilter)).subscribe({ next: value => { this._filter = value; } }),
      this._store.pipe(select(state => state.filter.currentSelect))
        .subscribe({ next: value => { this.isFavorite = value === ESelectedItems.favorite; } }),
      this._store.pipe(select(state => state.gallery))
        .subscribe({ next: value => { this.isMaxPage = value.page > value.totalPages; } })
    );
    if (window) {
      window.addEventListener('storage', this._onStorageChange);
    }

    const favoriteItems = this._webStore.get<DisplayData[]>(WebStorageService.FAVORITE);
    if (favoriteItems && favoriteItems.length) {
      this._store.dispatch(new InitFavorite(favoriteItems));
    }
  }

  ngOnDestroy(): void {
    for (const subscription of this._subscriptions) {
      subscription.unsubscribe();
    }
    if (window) {
      window.removeEventListener('storage', this._onStorageChange);
    }
  }

  loadMore(): void {
    this._filter === ''
      ? this._store.dispatch(new GetItems())
      : this._store.dispatch(new SearchItems(this._filter));
  }
}
