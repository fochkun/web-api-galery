import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.state';

@Component({
  selector: 'app-favorite-icon',
  templateUrl: './favorite-icon.component.html',
  styleUrls: ['./favorite-icon.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FavoriteIconComponent  {

  @Input() isFavorite: boolean;
  @Output() onChange: EventEmitter<boolean> = new EventEmitter();

  constructor(private _store: Store<AppState>) { }

  onStarClick(): void {
    this.isFavorite = !this.isFavorite;
    this.onChange.emit(this.isFavorite);
  }

}
