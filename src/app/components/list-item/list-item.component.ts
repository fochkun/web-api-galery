import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { DisplayData } from 'src/app/model/types-interfaces';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.state';
import { AddFavorite, DeleteFavorite } from 'src/app/store/favorite/favorite.actions';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListItemComponent {

  @Input() item: DisplayData;

  constructor(private _store: Store<AppState>) { }

  favoriteChange(isFavorite: boolean): void {
    isFavorite
      ? this._store.dispatch(new AddFavorite(this.item))
      : this._store.dispatch(new DeleteFavorite(this.item));
  }

}
