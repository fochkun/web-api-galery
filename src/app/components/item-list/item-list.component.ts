import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { DisplayData, ESelectedItems } from 'src/app/model/types-interfaces';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { AppState } from 'src/app/store/app.state';
import { selectGalleryItems } from 'src/app/store/gallery/gallery.selectors';
import { selectFavorite } from 'src/app/store/favorite/favorite.selector';
import { GetItems } from 'src/app/store/gallery/gallery.actions';
import { selectCurrentItems } from 'src/app/store/filter/filter.selector';

@Component({
    selector: 'app-item-list',
    templateUrl: './item-list.component.html',
    styleUrls: ['./item-list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemListComponent implements OnInit, OnDestroy {

    items: Observable<DisplayData[]> = this._store.pipe(select(selectGalleryItems));
    favorite: Observable<DisplayData[]> = this._store.pipe(select(selectFavorite));
    selectedItems = this.items;
    private _subscriptions: Subscription[] = [];

    constructor(private _store: Store<AppState>, private _cdr: ChangeDetectorRef) { }

    ngOnInit(): void {
        this._store.dispatch<GetItems>(new GetItems());
        this._subscriptions.push(
            this._store.pipe(select(selectCurrentItems)).subscribe(value => {
                this._cdr.markForCheck();
                this.selectedItems = value === ESelectedItems.favorite ? this.favorite : this.items;
            })
        );

    }

    ngOnDestroy(): void {
        for (const subscr of this._subscriptions) {
            subscr.unsubscribe();
        }
    }

    trackByFn(index: number, item: DisplayData): string {
        return item.id;
    }

}
