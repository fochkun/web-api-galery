import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { SelectedItems, ESelectedItems } from 'src/app/model/types-interfaces';
import { AppState } from 'src/app/store/app.state';
import { SetFilter, SetSelect } from 'src/app/store/filter/filter.actions';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterComponent {

  filterValue: string;

  isFavorite = false;

  constructor(private _store: Store<AppState>) { }

  setFilter(): void {
    this._store.dispatch(new SetFilter(this.filterValue));
  }

  onFavoriteChange(event: Event): void {
    const result: SelectedItems = (event.target as HTMLInputElement).checked ? ESelectedItems.favorite : ESelectedItems.gallery;
    this._store.dispatch(new SetSelect(result));
  }
}
