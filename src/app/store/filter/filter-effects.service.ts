import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { ApiService } from 'src/app/services/api.service';
import { map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { AppState, initialAppState } from '../app.state';
import { SetFilter, EFilterActions } from './filter.actions';
import { ResetPage } from '../gallery/gallery.actions';

@Injectable({
  providedIn: 'root'
})
export class FilterEffectsService {

  private _state: AppState = initialAppState;

  @Effect({ dispatch: false })
  setFilter$ = this._actions.pipe(
    ofType<SetFilter>(EFilterActions.SetFilter),
    map(action => {
      if (action.payload === this._state.filter.filter) {
        this._store.dispatch(new ResetPage(action.payload));
      }
    })
  );

  constructor(private _actions: Actions, private _api: ApiService, private _store: Store<AppState>) {
    this._store.pipe(select<AppState, unknown, AppState>(state => state)).subscribe({ next: state => { this._state = state; } });
  }
}
