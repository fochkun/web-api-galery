import { SelectedItems, ESelectedItems } from 'src/app/model/types-interfaces';

export interface FilterState {
    filter: string;
    currentSelect: SelectedItems;
}

export const initialFilterState: FilterState = {
    filter: '',
    currentSelect: ESelectedItems.gallery,
};
