import { Selector, createSelector } from '@ngrx/store';
import { AppState } from '../app.state';
import { FilterState } from './filter.state';


const filterState: Selector<AppState, FilterState> = state => state.filter;

export const selectFilter = createSelector(
    filterState,
    (state: FilterState) => state.filter
);

export const selectCurrentItems = createSelector(
    filterState,
    (state: FilterState) => state.currentSelect
);
