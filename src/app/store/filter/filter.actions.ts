import { Action } from '@ngrx/store';
import { SelectedItems } from 'src/app/model/types-interfaces';

export enum EFilterActions {
    SetFilter = '[filter] set filter',
    SetSelect = '[filter] set current select',
}

export class SetFilter implements Action {
    public readonly type = EFilterActions.SetFilter;
    constructor(public payload: string) { }
}

export class SetFilterSuccess implements Action {
    public readonly type = EFilterActions.SetFilter;
    constructor(public payload: string) { }
}

export class SetSelect implements Action {
    public readonly type = EFilterActions.SetSelect;
    constructor(public payload: SelectedItems) { }
}

export type FilterActions = SetFilter | SetSelect;
