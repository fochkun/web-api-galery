import { ReducerFunction } from 'src/app/model/types-interfaces';
import { FilterState, initialFilterState } from './filter.state';
import { FilterActions, EFilterActions, SetSelect } from './filter.actions';


type FilterReducer = ReducerFunction<FilterState, FilterActions>;

const reducers: Record<EFilterActions, FilterReducer> = {
    [EFilterActions.SetSelect]: (state: FilterState, action: SetSelect) => {
        return { ...state, currentSelect: action.payload };
    },
    [EFilterActions.SetFilter]: (state: FilterState, action: SetSelect) => {
        return { ...state, filter: action.payload };
    },
};

export const filterReducers: FilterReducer = (state = initialFilterState, action) => {
    return reducers[action.type] ? reducers[action.type](state, action) : state;
};
