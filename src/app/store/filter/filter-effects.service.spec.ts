import { TestBed } from '@angular/core/testing';

import { FilterEffectsService } from './filter-effects.service';

describe('FilterEffectsService', () => {
  let service: FilterEffectsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FilterEffectsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
