import { TestBed } from '@angular/core/testing';

import { FavoriteEffectsService } from './favorite-effects.service';

describe('FavoriteEffectsService', () => {
  let service: FavoriteEffectsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FavoriteEffectsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
