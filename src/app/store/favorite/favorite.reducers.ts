import { ReducerFunction, DisplayData } from 'src/app/model/types-interfaces';
import { FavoriteState, initialFavoriteState } from './favorite.state';
import { FavoriteActions, EFavoriteActions, InitFavorite, AddFavorite } from './favorite.actions';

type FavoriteReducer = ReducerFunction<FavoriteState, FavoriteActions>;

const reducers: Record<EFavoriteActions, FavoriteReducer> = {
    [EFavoriteActions.InitFavorite]: (state: FavoriteState, action: InitFavorite) => {
        return { items: [...action.payload] };
    },
    [EFavoriteActions.AddFavorite]: (state: FavoriteState, action: AddFavorite) => {
        const excludePayload = state.items.filter(item => item.id !== action.payload.id);
        return { items: [...excludePayload, { ...action.payload, isFavorite: true }] };
    },
    [EFavoriteActions.DeleteFavorite]: (state: FavoriteState, action: AddFavorite) => {
        return { items: state.items.filter(item => item.id !== action.payload.id) };
    }
};

export const favoriteReducers: FavoriteReducer = (state = initialFavoriteState, action) => {
    return reducers[action.type] ? reducers[action.type](state, action) : state;
};
