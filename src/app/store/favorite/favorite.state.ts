import { DisplayData } from 'src/app/model/types-interfaces';

export interface FavoriteState {
    items: DisplayData[];
}

export const initialFavoriteState: FavoriteState = {
    items: [],
};
