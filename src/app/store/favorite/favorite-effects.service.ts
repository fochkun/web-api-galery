import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { WebStorageService } from 'src/app/services/web-storage.service';
import { DisplayData } from 'src/app/model/types-interfaces';
import { AppState } from '../app.state';
import { AddFavorite, EFavoriteActions, DeleteFavorite, InitFavorite } from './favorite.actions';
import { selectFavorite } from './favorite.selector';
import { selectGalleryItems } from '../gallery/gallery.selectors';
import { ChangeItem } from '../gallery/gallery.actions';

@Injectable({
  providedIn: 'root'
})
export class FavoriteEffectsService {

  private _favorites: DisplayData[] = [];
  private _items: DisplayData[] = [];


  @Effect({ dispatch: false })
  addFavorite$ = this._actions.pipe(
    ofType<AddFavorite>(EFavoriteActions.AddFavorite),
    map(() => { this.saveFavorites(); })
  );

  @Effect({ dispatch: false })
  deleteFavorite$ = this._actions.pipe(
    ofType<DeleteFavorite>(EFavoriteActions.DeleteFavorite),
    map(() => { this.saveFavorites(); })
  );

  @Effect({ dispatch: false })
  initFavorites$ = this._actions.pipe(
    ofType<InitFavorite>(EFavoriteActions.InitFavorite),
    map((action) => {
      const changedItems = [...this._items].filter(item => {
        const changedFavorite = action.payload.find(favorite => {
          return item.id === favorite.id && !FavoriteEffectsService.isEquivalent(item, favorite);
        });
        return changedFavorite == undefined ? item.isFavorite && !action.payload.find(fav => fav.id === item.id) : true;
      });
      changedItems.forEach(item => {
        this._store.dispatch(new ChangeItem({ ...item, isFavorite: !item.isFavorite }));
      });
    })
  );

  static isEquivalent(a: DisplayData, b: DisplayData): boolean {
    return a.isFavorite === b.isFavorite;
  }


  constructor(private _actions: Actions, private _store: Store<AppState>, private _webStore: WebStorageService) {
    this._store.pipe(select(selectFavorite)).subscribe({ next: favorites => { this._favorites = favorites; } });
    this._store.pipe(select(selectGalleryItems)).subscribe({ next: items => { this._items = items; } });
  }

  private saveFavorites(): void {
    this._webStore.set(WebStorageService.FAVORITE, [...this._favorites]);
  }

}
