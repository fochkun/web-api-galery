import { Selector, createSelector } from '@ngrx/store';
import { AppState } from '../app.state';
import { FavoriteState } from './favorite.state';


const favoriteState: Selector<AppState, FavoriteState> = state => state.favorite;

export const selectFavorite = createSelector(
    favoriteState,
    (state: FavoriteState) => state.items
);
