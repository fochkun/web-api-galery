import { Action } from '@ngrx/store';
import { DisplayData } from 'src/app/model/types-interfaces';

export enum EFavoriteActions {
    InitFavorite = '[favorite] init favorite',
    AddFavorite = '[favorite] add favorite',
    DeleteFavorite = '[favorite] delete favorite',
}


export class InitFavorite implements Action {
    public readonly type = EFavoriteActions.InitFavorite;
    constructor(public payload: DisplayData[]) { }
}

export class AddFavorite implements Action {
    public readonly type = EFavoriteActions.AddFavorite;
    constructor(public payload: DisplayData) { }
}

export class DeleteFavorite implements Action {
    public readonly type = EFavoriteActions.DeleteFavorite;
    constructor(public payload: DisplayData) { }
}


export type FavoriteActions = AddFavorite | DeleteFavorite | InitFavorite;
