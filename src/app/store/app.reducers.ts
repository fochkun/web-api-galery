import { ActionReducerMap } from '@ngrx/store';
import { AppState } from './app.state';
import { galleryReducers } from './gallery/gallery.reducer';
import { favoriteReducers } from './favorite/favorite.reducers';
import { routerReducer } from '@ngrx/router-store';
import { filterReducers } from './filter/filter.reducers';


export const appReducers: ActionReducerMap<AppState, any> = {
    gallery: galleryReducers,
    favorite: favoriteReducers,
    router: routerReducer,
    filter: filterReducers
};
