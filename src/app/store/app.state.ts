import {RouterReducerState } from '@ngrx/router-store';
import { GalleryState, initialGalleryState } from './gallery/gallery.state';
import { FavoriteState, initialFavoriteState } from './favorite/favorite.state';
import { FilterState, initialFilterState } from './filter/filter.state';

export interface AppState {
    router?: RouterReducerState;
    gallery: GalleryState;
    favorite: FavoriteState;
    filter: FilterState;
}

export const initialAppState: AppState = {
    gallery: initialGalleryState,
    favorite: initialFavoriteState,
    filter: initialFilterState,

};

export function getInitialState(): AppState {
    return initialAppState;
}

