import { Selector, createSelector } from '@ngrx/store';
import { AppState } from '../app.state';
import { GalleryState } from './gallery.state';


const galleryState: Selector<AppState, GalleryState> = state => state.gallery;

export const selectGalleryItems = createSelector(
    galleryState,
    (state: GalleryState) => state.items
);

export const selectPage = createSelector(
    galleryState,
    (state: GalleryState) => state.page
);
