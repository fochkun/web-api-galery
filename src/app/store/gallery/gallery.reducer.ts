import { ReducerFunction, DisplayData } from 'src/app/model/types-interfaces';
import { GalleryState, initialGalleryState } from './gallery.state';
import { GalleryActions, EGalleryActions, GetItemsSuccess, SearchItemsSuccess, ChangeItem } from './gallery.actions';


type GalleryReducer = ReducerFunction<GalleryState, GalleryActions>;

const reducers: Record<EGalleryActions, GalleryReducer> = {
    [EGalleryActions.GetItems]: (state) => {
        return { ...state };
    },
    [EGalleryActions.GetItemsSuccess]: (state, action: GetItemsSuccess) => {
        const items: DisplayData[] = state.page === 1 ? [...action.payload] : [...state.items, ...action.payload];
        return { ...state, page: ++state.page, items, totalPages: initialGalleryState.totalPages };
    },
    [EGalleryActions.ResetPage]: (state) => {
        return { ...state, page: initialGalleryState.page };
    },
    [EGalleryActions.SearchItems]: (state) => {
        return { ...state };
    },
    [EGalleryActions.SearchItemsSuccess]: (state, action: SearchItemsSuccess) => {
        const items: DisplayData[] = state.page === 1 ? [...action.payload.items] : [...state.items, ...action.payload.items];
        return { ...state, page: ++state.page, items, totalPages: action.payload.totalPages };
    },
    [EGalleryActions.ChangeItem]: (state, action: ChangeItem) => {
        return { ...state, items: state.items.map(value => value.id === action.payload.id ? action.payload : value) };
    }
};

export const galleryReducers: GalleryReducer = (state = initialGalleryState, action) => {
    return reducers[action.type] ? reducers[action.type](state, action) : state;
};
