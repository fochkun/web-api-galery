import { DisplayData } from 'src/app/model/types-interfaces';

export interface GalleryState {
    items: DisplayData[];
    loading: boolean;
    page: number;
    totalPages: number;
}

export const initialGalleryState: GalleryState = {
    items: [],
    loading: false,
    page: 1,
    totalPages: Number.MAX_SAFE_INTEGER
};
