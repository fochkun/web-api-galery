import { Action } from '@ngrx/store';
import { DisplayData } from 'src/app/model/types-interfaces';

export enum EGalleryActions {
    GetItems = '[gallery] get items',
    GetItemsSuccess = '[gallery] get items success',
    ResetPage = '[gallery] reset page',
    SearchItems = '[gallery] search items',
    SearchItemsSuccess = '[gallery] search items success',
    ChangeItem = '[gallery] change item'

}

export class GetItems implements Action {
    public readonly type = EGalleryActions.GetItems;
}

export class GetItemsSuccess implements Action {
    public readonly type = EGalleryActions.GetItemsSuccess;
    constructor(public payload: DisplayData[]) { }
}

export class SearchItems implements Action {
    public readonly type = EGalleryActions.SearchItems;
    constructor(public payload: string) { }
}

export class SearchItemsSuccess implements Action {
    public readonly type = EGalleryActions.SearchItemsSuccess;
    constructor(public payload: { totalPages: number, items: DisplayData[] }) { }
}

export class ChangeItem implements Action {
    public readonly type = EGalleryActions.ChangeItem;
    constructor(public payload: DisplayData) { }
}

export class ResetPage implements Action {
    public readonly type = EGalleryActions.ResetPage;
    constructor(public payload: string = '') {}
}

export type GalleryActions = GetItems | GetItemsSuccess | ResetPage | SearchItems | SearchItemsSuccess | ChangeItem;
