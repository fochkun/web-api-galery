import { TestBed } from '@angular/core/testing';

import { GalleryEffectsService } from './gallery-effects.service';

describe('GalleryEffectsService', () => {
  let service: GalleryEffectsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GalleryEffectsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
