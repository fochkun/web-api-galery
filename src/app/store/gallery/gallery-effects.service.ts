import { Injectable } from '@angular/core';
import { AppState, initialAppState } from '../app.state';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { GetItems, EGalleryActions, GetItemsSuccess, SearchItems, SearchItemsSuccess, ResetPage } from './gallery.actions';
import { switchMap } from 'rxjs/operators';
import { DisplayData } from 'src/app/model/types-interfaces';
import { of } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { Store, select } from '@ngrx/store';


@Injectable({
  providedIn: 'root'
})
export class GalleryEffectsService {

  private _state: AppState = initialAppState;


  @Effect()
  getItems$ = this._actions.pipe(
    ofType<GetItems>(EGalleryActions.GetItems),
    switchMap(() => this._api.getPictures()),
    switchMap(items => {
      const result: DisplayData[] = items.map<DisplayData>(item => {
        return {
          id: item.id || '',
          name: item.user && item.user.name || '',
          description: item.description || '',
          cover: item.urls && item.urls.small || '',
          url: item.urls && item.urls.full || '',
          isFavorite: this.isFavorite(item.id),
        };
      });
      return of(new GetItemsSuccess(result));
    }),
  );

  @Effect()
  searchItems$ = this._actions.pipe(
    ofType<SearchItems>(EGalleryActions.SearchItems),
    switchMap(() => this._api.searchPictures(this._state.filter.filter)),
    switchMap(items => {
      const result: { totalPages: number, items: DisplayData[] } = {
        totalPages: items.total_pages,
        items: items.results.map<DisplayData>(item => {
          return {
            id: item.id || '',
            name: item.user && item.user.name || '',
            description: item.description || '',
            cover: item.urls && item.urls.small || '',
            url: item.urls && item.urls.full || '',
            isFavorite: this.isFavorite(item.id),
          };
        })
      };
      return of(new SearchItemsSuccess(result));
    }),
  );

  @Effect()
  resetPage$ = this._actions.pipe(
    ofType<ResetPage>(EGalleryActions.ResetPage),
    switchMap(action => of(action.payload === '' ? new GetItems() : new SearchItems(action.payload)))
  );


  constructor(private _actions: Actions, private _api: ApiService, private _store: Store<AppState>) {
    this._store.pipe(select<AppState, unknown, AppState>(state => state)).subscribe({ next: state => { this._state = state; } });
  }

  // check favorites array to have item with similar id
  private isFavorite(id: string): boolean {
    return this._state.favorite.items.map<string>(favorite => favorite.id).includes(id);
  }
}
