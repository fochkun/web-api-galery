export interface DisplayData {
    id: string;
    name: string;
    description: string;
    cover: string;
    url?: string;
    isFavorite: boolean;
}

export type ReducerFunction<StateType, ActionType> = (state: StateType, action: ActionType) => StateType;

export enum ESelectedItems {
    gallery= 'gallery',
    favorite = 'favorite'
}

export interface UnsplashData {
    id: string;
    user: { name: string };
    description: string;
    urls: { raw: string, full: string, regular: string, small: string, thumb: string };
  }

export type SelectedItems = ESelectedItems.gallery | ESelectedItems.favorite;
