export const enum API_ENDPOINTS {
    pictures = 'https://api.unsplash.com/photos/',
    search = 'https://api.unsplash.com/search/photos',
}
