import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './pages/index/index.component';
import { ItemListComponent } from './components/item-list/item-list.component';
import { FilterComponent } from './components/filter/filter.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { ApiService } from './services/api.service';
import { FavoriteIconComponent } from './components/favorite-icon/favorite-icon.component';
import { FormsModule } from '@angular/forms';
import { CropCharPipe } from './pipes/crop-char.pipe';
import { appReducers } from './store/app.reducers';
import { GalleryEffectsService } from './store/gallery/gallery-effects.service';
import { FilterEffectsService } from './store/filter/filter-effects.service';
import { FavoriteEffectsService } from './store/favorite/favorite-effects.service';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    ItemListComponent,
    FilterComponent,
    ListItemComponent,
    FavoriteIconComponent,
    CropCharPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot(appReducers),
    EffectsModule.forRoot([GalleryEffectsService, FilterEffectsService, FavoriteEffectsService]),
    StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
