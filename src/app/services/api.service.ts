import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_ENDPOINTS } from '../model/constants';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppState } from '../store/app.state';
import { initialGalleryState } from '../store/gallery/gallery.state';
import { selectPage } from '../store/gallery/gallery.selectors';
import { UnsplashData } from '../model/types-interfaces';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private _page: number = initialGalleryState.page;
  get page(): number {
    return this._page;
  }

  constructor(private http: HttpClient, private _store: Store<AppState>) {
    this._store.pipe(select(selectPage)).subscribe({ next: value => { this._page = value; } });
  }

  getPictures(filter?: string): Observable<UnsplashData[]> {
    return this.http.get<UnsplashData[]>(API_ENDPOINTS.pictures, {
      responseType: 'json',
      headers: { Authorization: 'Client-ID TXonhNcg_XWT-eh-iTW5BUSAD9OHFrjS4lcdeJ-IDCs' },
      params: { page: '' + this._page }
    });
  }

  searchPictures(filter = ''): Observable<{ total_pages: number, results: UnsplashData[] }> {
    const params = {};
    params['page'] = this.page;
    // tslint:disable-next-line: no-unused-expression
    filter && filter !== '' ? params['query'] = '' + this.page : true;
    return this.http.get<{ total_pages: number, results: UnsplashData[] }>(`${API_ENDPOINTS.search}?page=${this._page}&query=${filter}`, {
      responseType: 'json',
      headers: { Authorization: 'Client-ID TXonhNcg_XWT-eh-iTW5BUSAD9OHFrjS4lcdeJ-IDCs' },
      params
    });
  }


}
