import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WebStorageService {

  public static readonly FAVORITE = 'FAVORITE ITEMS';

  constructor() { }

  set(key: string, data: any): WebStorageService {
    try {
      localStorage.setItem(key, JSON.stringify(data));
      return this;
    } catch (e) {
      console.error('Error saving to localStorage', e);
    }

  }

  get<T>(key: string): T {
    try {
      return JSON.parse(localStorage.getItem(key));
    } catch (e) {
      console.error('Error getting data from localStorage', e);
      return null;
    }
  }
}
